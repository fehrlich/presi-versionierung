@echo off

cd pdfs
del -h *
cd ..

for %%i in (svgs/*.svg) do docker run --rm -v .:/app -w /app minidocks/inkscape -f "svgs/%%i" -A "pdfs/%%iPdf"

cd pdfs
ren *.svgPdf *.pdf
cd ..